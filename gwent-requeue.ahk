#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Client
CoordMode, Pixel, Client
SetControlDelay, -1
#Include %A_ScriptDir%\libs\FindText.ahk

DECK_SELECTION="|<>*94$65.00zzk01zs0600TzU03zU04T0Dz00Dy3s0zsDyDzzwDy1zsTwTzzkzzXzsTszzz3zz7zszlzzy7zyDzkzXzzsTzwTzVz7zzkzzszz3y01zXzzlzz7w03z7zzXzwDs07yDzz7zsTlzzwDzyDzlzXzzwTzwTz1z7zzsTzszy7yDzzkzzlzsDwTzzkTzXz0zszzzkTk1s7zkDzzkS000zzU03zk0A0Dzz007zw3w"
IN_QUEUE="|<>*98$41.kA0S3s7080s3k2STDbbjVzyTDbTXzwyzCz7ztxyRyXznvwvxUzbrtrnkzDjnUDwyTTbTztwyzCzzvtxyRzzbnvwvyTDbnnry0zDl7jy3yTkzTs"
global RUN_SCRIPT

^F1::
	RUN_SCRIPT := True
	While, RUN_SCRIPT {
		WinActivate, Gwent

		if ok:=FindText(0,0,9000,9000,0.1,0.1,DECK_SELECTION) {
		  SendInput, {Enter}
		  SetTimer, RestartSearch, 10000 ;Restart gwent every 10000 msec
		}

		Sleep, 1000
	}
Return

RestartSearch:
	if RUN_SCRIPT {
		WinActivate, Gwent
		if ok:=FindText(0,0,9000,9000,0.1,0.1,IN_QUEUE) {
		  SendInput, {Escape}
		}
	}
Return

^F2::
	RUN_SCRIPT := False
Return

^F12::
	ExitApp
Return