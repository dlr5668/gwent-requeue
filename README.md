# About
This AHK script will restart gwent queue every 10 seconds to avoid extended search. 1080p only. Use utility/FindTextGUI.ahk if you need other resolution.

# Hotkeys

Ctrl + F1 - start search

Ctrl + F2 - end search

Ctrl + F12 - exit

# How I can modify it for another language / resolution

start utility/FindTextGUI.ahk -> catch

Capture this word https://lh3.googleusercontent.com/-A0dPT2VxuYw/WO6MlgsmxxI/AAAAAAAAPNc/5_PSSo1TT98/s0/Gwent_2017-04-12_23-22-43.png

Press Gray2Two -> Auto -> Ok
https://lh3.googleusercontent.com/-gcG1m8We_eU/WO6M1vftEkI/AAAAAAAAPNg/BaAePscMaXE/s0/AutoHotkey_2017-04-12_23-23-49.png

Copy this string https://lh3.googleusercontent.com/-gz0r3NkBNpw/WO6M6YL7NsI/AAAAAAAAPNk/uagfFDfjLqM/s0/AutoHotkey_2017-04-12_23-24-07.png

Repeat for IN_QUEUE 
https://lh3.googleusercontent.com/-7igJO02HX6U/WO6NSSAuKaI/AAAAAAAAPNo/K10YTLaYzYk/s0/Gwent_2017-04-12_23-25-39.png